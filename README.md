# Installation
  - You just need to run it as standart angular application. Also don't forget to add your addition file ../casr-forms.tgz.
# Authorization
  - For mock auth in this project used FireBase  auth. 
  - For login you can use two test account :
  >     1. test@mail.com, password 123456
  >     2. test2@mail.com, password 123456
  - If you enter incorrect data, you will see standard error messages from server
  - After successfull auth you will be redirect to my-work page. In header you can see your account name. On click - dropdown with Logout option.

