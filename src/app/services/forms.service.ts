import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Form } from '../models/form'
import { Router } from '@angular/router';

@Injectable()
export class FormsService {
    formList: Form[];

    constructor(
        private router: Router,

    ) {
        this.formList = [
            {
                Id: 11,
                formTitle: 'Global',
                subForms: ['Request Appeal on Reject Desicison', 'Cances Process', 'Create Account For Organization']
            },
            {
                Id: 12,
                formTitle: 'CL/Aircraft Registration',
                subForms: ['Request Appeal on Reject Desicison on Reject Desicison on Reject Desicison on Reject Desicison on Reject Desicison', 'Create Account For Organization', 'Create Account For Organization', 
                'Create Account For Organization', 'Create Account For Organization', 'Create Account For Organization', 'Create Account For Organization', 'Create Account For Organization']
            },
            {
                Id: 13,
                formTitle: 'CL/Knowledge tests',
                subForms: ['Request Appeal on Reject Desicison', 'Cances Process']
            }
        ]
    }

    getForms(): Observable<Form[]> {
        return of(this.formList)
    }


}





