import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Language } from '../models/language';
import { Router } from '@angular/router';

@Injectable()
export class TranslateService{
    data: any = {};
    LanguageList: Language[];
    public onUseLanguage: EventEmitter<any>;

    constructor(private http: HttpClient)
      {
      this.LanguageList = [{
          Id: 1,
          Language: "Latin",
          Abbreviation: "la"
        },
        {
          Id: 2,
          Language: "Arabic",
          Abbreviation: "ar"
        },
        {
          Id: 3,
          Language: "Cyrillic",
          Abbreviation: "cy"
        },
        {
          Id: 4,
          Language: "Mandarin",
          Abbreviation: "ma"
        },
      ]
      this.onUseLanguage = new EventEmitter();
    }



    use(lang: string): Promise<{}> {
      this.LanguageList.forEach(element => {
        if (element.Language == lang)
        lang = element.Abbreviation;
      });
        return new Promise<{}>((resolve, reject) => {
          const langPath = `assets/i18n/${lang || 'la'}.json`;
          this.http.get<{}>(langPath).subscribe(
            translation => {
              this.data = Object.assign({}, translation || {});
              resolve(this.data);
              this.onUseLanguage.emit(this.data);
            },
            error => {
              this.data = {};
              resolve(this.data);
              this.onUseLanguage.emit(this.data);
            }
          );
        });
    }

    getLanguage(){
      let lang = this.data;
      return lang;
    }
}