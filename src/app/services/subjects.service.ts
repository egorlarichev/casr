import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';

@Injectable()
export class ClearFilterSubject {
    private subject = new Subject<any>();

    clearFilter(obj: any) {
        this.subject.next(obj);
    }

    clearMessage() {
        this.subject.next();
    }

    detectFilterClear(): Observable<any> {
        return this.subject.asObservable();
    }

}
