import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Request } from '../models/request'
import { Router } from '@angular/router';

@Injectable()
export class RequestService {
  requestList: Request[];

  constructor(
    private router:Router,

  ) {
    this.requestList = [
      {
        Id: 11,
        IsUrgent: true,
        ProcessName: 'Affiliation Approval',
        BpmProcessId: '123123sadaDDADSasd1e1231',
        FormType: ['Form Type 1', 'Form Type 2'],
        FormVersion: '1.0',
        RequestNumber: "P.GO.2018.000150",
        StartDate: new Date(2017 - 12 - 20),
        StartTime: "15:00",
        EndDate: new Date(2017 - 10 - 20),
        EndTime: '17:00',
        Status: 'Completed',
        DateStatus: new Date(2017 - 12 - 20),
        TimeStatus: "15:00",
        Departament: ['Department 1', 'Department 2',],
        Entity: ['Entity 1', 'Entity 2'],
        Type: 1,
        Applicant: ['Applicant 1', 'Applicant 2'],
      },
      {
        Id: 12,
        IsUrgent: true,
        ProcessName: 'Affiliation Approval',
        BpmProcessId: '123123sadaDDADSasd1e1231',
        FormType: ['Form Type 1', 'Form Type 2'],
        FormVersion: '1.0',
        RequestNumber: "P.GO.2018.000150",
        StartDate: new Date(2017 - 12 - 20),
        StartTime: "15:00",
        EndDate: new Date(2017 - 10 - 20),
        EndTime: '17:00',
        Status: 'Completed',
        DateStatus: new Date(2017 - 12 - 20),
        TimeStatus: "15:00",
        Departament: ['Department 1', 'Department 2',],
        Entity: ['Entity 1', 'Entity 2'],
        Type: 1,
        Applicant: ['Applicant 1', 'Applicant 2'],
      },
      {
        Id: 13,
        IsUrgent: true,
        ProcessName: 'Affiliation Approval',
        BpmProcessId: '123123sadaDDADSasd1e1231',
        FormType: ['Form Type 1', 'Form Type 2'],
        FormVersion: '1.0',
        RequestNumber: "P.GO.2018.000150",
        StartDate: new Date(2017 - 12 - 20),
        StartTime: "15:00",
        EndDate: new Date(2017 - 10 - 20),
        EndTime: '17:00',
        Status: 'Completed',
        DateStatus: new Date(2017 - 12 - 20),
        TimeStatus: "15:00",
        Departament: ['Department 1', 'Department 2',],
        Entity: ['Entity 1', 'Entity 2'],
        Type: 1,
        Applicant: ['Applicant 1', 'Applicant 2'],
      },
    ]
  }

  getActivity(): Observable<Request[]> {
    // TO DO: Change to real API REQUEST
    return of(this.requestList)
  }

  getActivityDetail(id): Observable<Request> {
    // TO DO: Change to real API REQUEST
    var detail = this.requestList.find(object => object.Id == id) as Request
    if (!detail) {
      this.router.navigate(['/sec/requests'])
      return
    } 
    return of(detail)
  }





}





