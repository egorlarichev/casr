import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Activity } from '../models/activity'
import { Router } from '@angular/router';

@Injectable()
export class ActivityService {
    ActivityList: Activity[];

    constructor(
        private router: Router,

    ) {
        this.ActivityList = [
            {
                Id: 11,
                IsUrgent: true,
                Name: 'Affiliation Approval',
                FormType: 'Test From Type',
                RequestNumber: "P.GO.2018.000150",
                StartDate: new Date(2017 - 12 - 20),
                StartTime: "15:00",
                ToBeDoneBy: 'casradmin-IT Employer',
                ConfirmedDate: new Date(2017 - 10 - 20),
                ConfirmedTime: '17:00',
                Applicant: 'First Name',
                Entity: 'Sdasd',
                Type: 1,
            },
            {
                Id: 12,
                IsUrgent: true,
                Name: 'Affiliation Approval',
                FormType: 'Test From123123 Type',
                RequestNumber: "P.GO.2018.000150",
                StartDate: new Date(2017 - 12 - 20),
                StartTime: "15:00",
                ToBeDoneBy: 'casradmin-IT Employer',
                ConfirmedDate: new Date(2017 - 10 - 20),
                ConfirmedTime: '17:00',
                Applicant: 'First Name',
                Entity: 'Sdasd',
                Type: 1,

            },
            {
                Id: 13,
                IsUrgent: true,
                Name: 'Affiliation Ap13123proval',
                FormType: 'Test From Type',
                RequestNumber: "P.GO.212313018.000150",
                StartDate: new Date(2017 - 12 - 20),
                StartTime: "15:00",
                ToBeDoneBy: 'casradmin-IT Employer',
                ConfirmedDate: new Date(2017 - 10 - 20),
                ConfirmedTime: '12:00',
                Applicant: 'First Name',
                Entity: 'Sdasd',
                Type: 1,

            },
            {
                Id: 14,
                IsUrgent: false,
                Name: 'Affiliation Ap13123proval',
                FormType: 'Test From Type',
                RequestNumber: "P.GO.212313018.000150",
                StartDate: new Date(2017 - 12 - 20),
                StartTime: "15:00",
                ToBeDoneBy: 'casradmin-IT Employer',
                ConfirmedDate: new Date(2017 - 10 - 20),
                ConfirmedTime: '12:00',
                Applicant: 'First Name',
                Entity: 'Sdasd',
                Type: 1,

            },
            {
                Id: 15,
                IsUrgent: true,
                Name: 'Affiliation Ap13123proval',
                FormType: 'Test From Type',
                RequestNumber: "P.GO.212313018.000150",
                StartDate: new Date(2017 - 12 - 20),
                StartTime: "15:00",
                ToBeDoneBy: 'casradmin-IT Employer',
                ConfirmedDate: new Date(2017 - 10 - 20),
                ConfirmedTime: '12:00',
                Applicant: 'First Name',
                Entity: 'Sdasd',
                Type: 1,

            },
            {
                Id: 16,
                IsUrgent: true,
                Name: 'Affiliation Ap13123proval',
                FormType: 'Test From Type',
                RequestNumber: "P.GO.212313018.000150",
                StartDate: new Date(2017 - 12 - 20),
                StartTime: "15:00",
                ToBeDoneBy: 'casradmasdadain-IT Employer',
                ConfirmedDate: new Date(2017 - 10 - 20),
                ConfirmedTime: '12:00',
                Applicant: 'Firstasdasdasd Name',
                Entity: 'Sdasd',
                Type: 1,

            },
        ]
    }

    getActivity(): Observable<Activity[]> {
        // TO DO: Change to real API REQUEST
        return of(this.ActivityList)
    }

    getActivityDetail(id): Observable<Activity> {
        // TO DO: Change to real API REQUEST

        console.log(id)
        var detail = this.ActivityList.find(object => object.Id == id) as Activity
        if (!detail) {
            this.router.navigate(['./sec'])
            return
        }
        return of(detail)
    }





}





