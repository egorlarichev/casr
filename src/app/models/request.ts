export class Request {
  Id: number;
  IsUrgent: boolean;
  ProcessName: string;
  BpmProcessId: string;
  FormType: Array<string>;
  FormVersion: string;
  RequestNumber: string;
  StartDate: Date;
  StartTime: string;
  EndDate: Date;
  EndTime: string;
  Status: string;
  DateStatus: Date;
  TimeStatus: string;
  Departament: Array<string>;
  Entity: Array<string>;
  Type: number;
  Applicant: Array<string>;
}