export class Activity {
  Id: number;
  IsUrgent: boolean;
  Name: string;
  FormType: string;
  RequestNumber: string;
  StartDate: Date;
  StartTime: string;
  ToBeDoneBy: String;
  ConfirmedDate: Date;
  ConfirmedTime: string;
  Applicant: string;
  Entity: string;
  Type: number;
}