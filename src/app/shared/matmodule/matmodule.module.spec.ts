import { MatModule } from './matmodule.module';

describe('MatmoduleModule', () => {
  let matmoduleModule: MatModule;

  beforeEach(() => {
    matmoduleModule = new MatModule();
  });

  it('should create an instance', () => {
    expect(matmoduleModule).toBeTruthy();
  });
});
