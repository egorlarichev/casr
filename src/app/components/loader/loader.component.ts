import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../../services/loader.service';
import 'zone.js';
@Component({
  selector: 'loading-bar',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  public active: boolean;

  public constructor(loaderservice: LoaderService) {
    loaderservice.status.subscribe((status: boolean) => {
      this.active = status;
    });
  }

  ngOnInit() {
  }

}
