import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePasComponent } from './changePas.component';

describe('ChangePasComponent', () => {
  let component: ChangePasComponent;
  let fixture: ComponentFixture<ChangePasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangePasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
