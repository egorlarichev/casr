import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { LoaderService } from '../../../services/loader.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '../../../services/translate.service';
 

@Component({
  selector: 'app-ChangePas',
  templateUrl: './ChangePas.component.html',
  styleUrls: ['./ChangePas.component.css']
})
export class ChangePasComponent implements OnInit {
  language = this.translateService.getLanguage();
  public loginForm: FormGroup;
  resetPassword = false;
  email: any;
  isError: boolean = false;;

  ChangePassword: string = "Change Password";
  OldPassword: string = "Old password";
  NewPassword: string = "New password";
  RepeatPassword: string = "Repeat Password";
  constructor(
    private route: ActivatedRoute, 
    private router: Router, 
    private loader: LoaderService, 
    private authService: AuthService,
    private fb: FormBuilder,
    private translateService: TranslateService
    
  ) {
    this.loginForm = this.fb.group({
      'oldPassword': ['', Validators.required],
      'newPassword': ['', Validators.required],
      'repeatPassword': ['', Validators.required]
    });
  }

  error: string;

  
   ngOnInit() {
    this.translateService.onUseLanguage.subscribe(
      language => this.onUsedLanguage(language)
    )
    this.onUsedLanguage(this.language);
   }

  updatePassword() {
      this.loader.start();

      const credentials = this.loginForm.value;

      this.authService.updatePassword(credentials)
      .then(response => {
        console.log(2)
        this.loader.stop();
        this.router.navigate(['./sec']);
      })
      .catch( error => {
        console.log(1)
        this.loader.stop();
        this.error = error.message;
      });

   }

   checkPassword() {
     var newPassword = this.loginForm.controls.newPassword.value
     var oldPassword = this.loginForm.controls.repeatPassword.value
     if (newPassword != oldPassword) {
     this.isError = true;
     }
     else {
      this.isError = false;     }

    //  this.loginForm.controls.repeatPassword.updateValueAndValidity();


   }

   onUsedLanguage(language: any){
    this.ChangePassword = language["ChangePassword"];
    this.OldPassword = language["OldPassword"];
    this.NewPassword = language["NewPassword"];
    this.RepeatPassword = language["RepeatPassword"];

    
   }
  }