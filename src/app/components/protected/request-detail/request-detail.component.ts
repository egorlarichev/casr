import { Component, OnInit,Input } from '@angular/core';
import { Router, ActivatedRoute, Params, Data } from '@angular/router';
import { RequestService } from '../../../services/request.service';
import { Request } from '../../../models/request'
import { TranslateService } from '../../../services/translate.service';


@Component({
  selector: 'app-request-detail',
  templateUrl: './request-detail.component.html',
  styleUrls: ['./request-detail.component.css']
})
export class RequestDetailComponent implements OnInit {
  language = this.translateService.getLanguage();
  requestService: RequestService
  requestId: string = this.route.snapshot.params['id'];
  requestDetail: Request; 
  breadcrumb$: any;
  pageHeader: string = this.route.snapshot.data['pageHeader']
  breadcrumb: string = this.route.snapshot.data['breadcrumb']

  // pageHeader: string = this.route.snapshot.data['pageHeader']
  // typeId: number = this.route.snapshot.data['typeId']

  ProcessName: string = "Process Name";
  BPMProcessId: string = "BPM Process Id";
  FormType: string = "Form Type";
  FormVersion: string = "Form Version";
  RequestNumber: string = "RequestNumber";
  StartDate: string = "Start Date";
  StartTime: string = "Start Time";
  EndDate: string = "End Date";
  EndTime: string = "End Time";
  Status: string = "Status";
  DateStatus: string = "Date Status";
  TimeStatus: string = "Time Status";
  Departament: string = "Departament";
  Entity: string = "Entity";
  Applicant: string = "Applicant";
  Invoices: string = "Invoices";
  Emails: string = "Emails";
  Letters: string = "Letters";
  Tasks: string = "Tasks";
  GeneratedInfo: string = "Generated Info";
  Notes: string = "Notes";
  ProcessDocuments: string = "Process Documents";
  ApplicantDocuments: string = "Applicant Documents";
  Requirements: string = "Requirements";
  SubmittedForm: string = "Submitted Form";


  constructor(requestyService: RequestService,
    private route: ActivatedRoute,
    private router: Router,
    private translateService: TranslateService) {
    this.requestService = requestyService;
  }

  private onUsedLanguage(language: any){

    this.FormType = language["FormType"];
    this.RequestNumber = language["RequestNumber"];
    this.StartDate = language["StartDate"];
    this.Entity  = language["Entity"];
    this.ProcessName = language["ProcessName"];
    this.BPMProcessId = language["BPMProcessId"];
    this.FormVersion = language["FormVersion"];
    this.StartTime = language["StartTime"];
    this.EndDate = language["EndDate"];
    this.EndTime = language["EndTime"];
    this.Status = language["Status"];
    this.DateStatus = language["DateStatus"]; 
    this.TimeStatus = language["TimeStatus"];  
    this.Departament = language["Departament"];  
    this.Applicant = language["Applicant"];
    this.Invoices = language["Invoices"];
    this.Emails = language["Emails"];
    this.Letters = language["Letters"];
    this.Tasks = language["Tasks"];
    this.GeneratedInfo = language["GeneratedInfo"];
    this.Notes = language["Notes"];
    this.ProcessDocuments= language["ProcessDocuments"];
    this.ApplicantDocuments = language["ApplicantDocuments"];
    this.Requirements = language["Requirements"];
    this.SubmittedForm = language["SubmittedForm"];
  }

  ngOnInit() {
    this.requestService.getActivityDetail(this.requestId).subscribe(
      data => {
        this.requestDetail = data
        console.log(data);
      }
    )

    
    var res = this.router.url.split("/");
    var string = res[0]+'/'+res[1]+'/'+res[2]

    this.breadcrumb$ = [{
      label: this.pageHeader,
      url: string
    },
    {
      label: this.breadcrumb,
      url: this.router.url,
    },]


    this.translateService.onUseLanguage.subscribe(
      language => this.onUsedLanguage(language)
    )
    this.onUsedLanguage(this.language);
  }


}
