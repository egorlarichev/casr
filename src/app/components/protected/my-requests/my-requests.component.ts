import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit} from '@angular/core';
import { Router, ActivatedRoute, Params, Data } from '@angular/router';

import { RequestService } from '../../../services/request.service';
import { Request } from '../../../models/request'
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import {merge} from "rxjs";
import {tap, delay} from "rxjs/operators";
import { ClearFilterSubject } from '../../../services/subjects.service';
import { TranslateService } from '../../../services/translate.service';

@Component({
  selector: 'app-my-requests',
  templateUrl: './my-requests.component.html',
  styleUrls: ['./my-requests.component.css']
})
export class MyRequestsComponent implements OnInit {
  language = this.translateService.getLanguage();
  requestService: RequestService
  requestList: Request[];
  dataSource = new MatTableDataSource();
  filterObject: any;
  clearFilterSubject: ClearFilterSubject

  // pageHeader: string = this.route.snapshot.data['pageHeader']
  // typeId: number = this.route.snapshot.data['typeId']

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ["ProcessName", "FormType", "RequestNumber", "StartDate", "EndDate", "Status", "Entity", "Applicant"];

  ProcessName:string = "Process Name";
  FormType: string = "Form Type";
  RequestNumber: string = "Request Number";
  StartDate: string = "Start Date";
  EndDate: string = "End Date";
  Status: string = "Status";
  Entity: string = "Entity";
  Applicant: string = "Applicant";
  ExportToExel: string = "Export to Exel";
  ClearAllFilters: string = "ClearAllFilters";
  breadcrumb$: any;
  pageHeader: string = this.route.snapshot.data['pageHeader']

  private usedLanguage: any;

  constructor(requestService: RequestService,
    private route: ActivatedRoute,
    private router: Router,
    clearFilterSubject: ClearFilterSubject,
    private translateService: TranslateService) {
    this.requestService = requestService;
    this.clearFilterSubject = clearFilterSubject;
  }

  private onUsedLanguage(language: any){

    this.FormType = language["FormType"];
    this.RequestNumber = language["RequestNumber"];
    this.StartDate = language["StartDate"];
    this.Entity  = language["Entity"];
    this.ProcessName = language["ProcessName"];
    this.EndDate = language["EndDate"];
    this.Status = language["Status"];
    this.Applicant = language["Applicant"];
    this.ExportToExel = language["ExportToExel"];
    this.ClearAllFilters = language["ClearAllFilters"];
  }

  ngOnInit() {

    this.filterObject = {
      // typeId: this.typeId,
       CurrentPage : 1,
       PageSize: 3,
       Items: 0,
       SortColumn: "",
       SortDirection: "",
       Filter: {},
    }

    this.breadcrumb$ = [{
      label: this.pageHeader,
      url: this.router.url,
    },]

    this.requestService.getActivity().subscribe(
      data => {
        this.dataSource.data = data
      });

      this.translateService.onUseLanguage.subscribe(
        language => this.onUsedLanguage(language)
      )
      this.onUsedLanguage(this.language);
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
  
    merge(this.sort.sortChange, this.paginator.page)
    .pipe(
        tap(() => {
          this.filterObject.CurrentPage = this.paginator.pageIndex;
          this.filterObject.PageSize = this.paginator.pageSize;
          this.filterObject.Items  = this.paginator._length;
          this.filterObject.SortColumn = this.sort.active;
          this.filterObject.SortDirection = this.sort.direction;
        }
      )
    )
    .subscribe();

    this.sort.sortChange.subscribe(() => {
      console.log(this.paginator)
      this.paginator.pageIndex = 0});

    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

  }


  onExportToExelClick() {
    alert('You want to export ' + this.pageHeader+'?')
  }

  onRowClicked(row) {
    this.router.navigate([this.router.url+'/details/'+row.Id]);
    console.log('Row clicked: ', row);
  }

  onApplyFilter(data) {
    //TO DO:
    // this.requestService.getActivity() 
    this.filterObject.Filter[data.fieldName] = data.filterData
    console.log(  this.filterObject)
    
  }

  onClearFilter(data) {
    //TO DO:
    // this.requestService.getActivity() 
   delete this.filterObject.Filter[data];
    console.log(  this.filterObject)
  }

  clearAllFilters() {
    //TO DO:
    // this.requestService.getActivity() 
    var object = {}
    this.clearFilterSubject.clearFilter(object);
    this.filterObject.Filter = {};
  }
}
