import { BrowserModule }  from '@angular/platform-browser';
import { Component, OnInit, ViewChild, ElementRef, Output } from '@angular/core';
import { LoaderService } from '../../services/loader.service';
import { ProfileService } from '../../services/profile.service';
import { Router, RouterState, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { TranslateService } from '../../services/translate.service';
import { Observable } from 'rxjs';



declare var $;

@Component({
  selector: 'irms-protected',
  templateUrl: './protected.component.html',
  styleUrls: ['./protected.component.css'],
})
export class ProtectedComponent implements OnInit {
  public activeLoader: boolean;
  public user;
  isHierarchyOpen: boolean = false;
  isMenuOpen: boolean = false;
  isLanguageOpen: boolean = false;
  language: string = "Latin";

  @ViewChild("sidenav") sidenav;
  @ViewChild("toggleButton") _toggleButton: ElementRef;

  private usedLanguage : any;

  constructor(
    private loader: LoaderService, 
    private profileService: ProfileService, 
    private router:Router,
    private authService: AuthService, 
    private translateService: TranslateService
  ) {
    
    loader.status.subscribe((status: boolean) => {
      this.activeLoader = status;
    });

    this.authService.userSub$.subscribe(user => {
      console.log(user)
      this.user = user;
    })
  }
 
  ngOnInit() {
    this.onUsedLanguage(this.translateService.getLanguage());
    //.onUseLanguage.subscribe(language => this.onUsedLanguage(language));
  }

  private onUsedLanguage(language: any){
    this.usedLanguage = language;
  }

  openHierarchy() {
    this.isHierarchyOpen = !this.isHierarchyOpen
  }

  openUserMenu() {
    this.isMenuOpen = !this.isMenuOpen
  }

  openLanguageMenu(){
    this.isLanguageOpen = !this.isLanguageOpen
  }

  goTo(){
    this.sidenav.close();
    console.log(this._toggleButton)

  }
  
  logOut(){
    this.authService.signOut();
    this.router.navigate(['/signin'])
  }

  SetLanguage(language: string){
    this.translateService.use(language);
    this.language = language;
  }
}

