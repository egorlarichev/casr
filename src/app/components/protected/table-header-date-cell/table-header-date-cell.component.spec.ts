import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableHeaderDateCellComponent } from './table-header-date-cell.component';

describe('TableHeaderDateCellComponent', () => {
  let component: TableHeaderDateCellComponent;
  let fixture: ComponentFixture<TableHeaderDateCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableHeaderDateCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableHeaderDateCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
