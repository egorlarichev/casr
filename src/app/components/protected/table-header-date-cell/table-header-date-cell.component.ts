import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ClearFilterSubject } from '../../../services/subjects.service';
import { TranslateService } from '../../../services/translate.service';

@Component({
  selector: 'app-table-header-date-cell',
  templateUrl: './table-header-date-cell.component.html',
  styleUrls: ['./table-header-date-cell.component.css']
})
export class TableHeaderDateCellComponent implements OnInit {
  language = this.translateService.getLanguage();
  @Input() cellName;
  @Input() fieldName;
  @Output() emitApplyFilter: EventEmitter<any> = new EventEmitter();
  @Output() emitClearFilter: EventEmitter<any> = new EventEmitter();

  Group: string = "Group";
  ContainsData: string = "Contains Data";
  DoesntContainsData: string = "Doesn`t Contains Data";
  Filter: string = "Filter";
  Before: string = "Before";
  After: string = "After";
  Between: string = "Between";
  Clear: string = "Clear";
  Apply: string = "Apply";
  Choose: string = "Choose";

  group: boolean = false;
  containsData: boolean = false;
  notContainsData: boolean = false;
  filter: boolean = false;
  radioValue: string = "";
  beforeDate: Date = null;
  afterDate: Date = null;
  betweenFirstDate: Date = null;
  betweenSecondDate: Date = null;

  isMenuOpen: boolean = false;
  HaveFilter: boolean = false;
  clearFilterSubject: ClearFilterSubject

  constructor(clearFilterSubject: ClearFilterSubject,
    private translateService: TranslateService) { 
    this.clearFilterSubject = clearFilterSubject
  }

  ngOnInit() {
    this.clearFilterSubject.detectFilterClear().subscribe(data => {
      console.log('clearFilter');
      this.onClear();
    })

    this.translateService.onUseLanguage.subscribe(
      language => this.onUsedLanguage(language)
    )
    this.onUsedLanguage(this.language);
  }

  onGroupChange(){
    if(this.group){
      this.containsData = false;
      this.notContainsData = false;
      this.filter = false;
      this.radioValue = "";
      this.afterDate = null;
      this.beforeDate = null;
      this.betweenFirstDate = null;
      this.betweenSecondDate = null;
    }
  }

  onChangeContainsData() {
    if (this.containsData) {
      this.group = false;
      this.notContainsData = false;
      this.filter = false;
      this.radioValue = "";
      this.afterDate = null;
      this.beforeDate = null;
      this.betweenFirstDate = null;
      this.betweenSecondDate = null;
    }
  }

  onChangeNotContainsData() {
    if (this.notContainsData) {
      this.group = false;
      this.containsData = false;
      this.filter = false;
      this.radioValue = "";
      this.afterDate = null;
      this.beforeDate = null;
      this.betweenFirstDate = null;
      this.betweenSecondDate = null;
    }
  }

  onFilterChange(){
    if (this.filter) {
      this.group = false;
      this.containsData = false;
      this.notContainsData = false;
    }
  }

  onClear() {
    var _this = this;

    this.HaveFilter = false
    this.containsData = false;
    this.notContainsData = false;
    this.filter = false;
    this.radioValue = "";
    this.afterDate = null;
    this.beforeDate = null;
    this.betweenFirstDate = null;
    this.betweenSecondDate = null;
    _this.emitClearFilter.emit(this.fieldName);
  }

  onApplyFilter() {
    var _this = this;
    this.HaveFilter = true

    let FilterObject = {
      fieldName: this.fieldName,
      filterData: {
        group: this.group,
        containsData: this.containsData,
        notContainsData: this.notContainsData,
        filter: this.filter,
        radioValue: this.radioValue,
        afterDate: this.afterDate,
        beforeDate: this.beforeDate,
        betweenFirstDate: this.betweenFirstDate,
        betweenSecondDate: this.betweenSecondDate
      }
    }
    _this.emitApplyFilter.emit(FilterObject);

  }

  onMenuChange() {
    this.isMenuOpen = !this.isMenuOpen;
  }

  onUsedLanguage(language: any){
    this.Group =  language["Group"];
    this.ContainsData = language["ContainsData"];
    this.DoesntContainsData = language["DoesntContainsData"];
    this.Filter = language["Filter"];
    this.Before = language["Before"];
    this.After  = language["After"];
    this.Between  = language["Between"];
    this.Clear  = language["Clear"];
    this.Apply  = language["Apply"];
    this.Choose = language["Choose"];
  }
}
