import { Component, OnInit } from '@angular/core';
import { PopupManager } from 'ng6-popup-boxes';
import { TranslateService } from '../../../services/translate.service';




export class LoadingComponent { }

@Component({ templateUrl: 'help.component.html' })
export class HelpComponent {
  language = this.translateService.getLanguage();

  Help:string = "Help";

  constructor(public popupManager: PopupManager,
    private translateService: TranslateService) {}
 
    ngOnInit() {
      this.translateService.onUseLanguage.subscribe(
        language => this.onUsedLanguage(language)
        )
        this.onUsedLanguage(this.language);
    }

    onUsedLanguage(language: any){
        this.Help = language["Help"];
    }

    showAlertBox() {
      this.popupManager.open(
        'okay',
        "Let's go up in here, and start having some fun The very fact that you're aware of suffering is enough reason to be overjoyed that you're alive and can experience it. It's a super day, so why not make a beautiful sky? ",
        {
          width: '300px',
          popupClass: 'my-popup-box',
          animate: 'slide-from-top',
          showOverlay: true,
          position: 'top',
          callback: (result: any) => {
            if (result) {
              this.customDialog('You clicked Ok');
            } else {
              this.customDialog('You clicked Cancel');
            }
          }
        }
      );
    }
  
    customDialog(message: any) {
      this.popupManager.open('Custom Dialog', message, {
        width: '300px',
        position: 'bottom',
        animate: 'scale',
        actionButtons: [
          {
            text: 'Done',
            buttonClasses: 'btn-ok'
          }
        ]
      });
    }
  
    showConfirmBox() {
      this.popupManager.open('Delete Confirmation', 'Do you really want to this item?', {
        width: '300px',
        closeOnOverlay: false,
        animate: 'scale',
        actionButtons: [
          {
            text: 'Yes',
            buttonClasses: 'btn-ok',
            onAction: () => {
              return true;
            }
          },
          {
            text: 'No',
            buttonClasses: 'btn-cancel',
            onAction: () => {
              return false;
            }
          }
        ],
        callback: (result: any) => {
          if (result) {
            this.showLoadingBox();
          }
        }
      });
    }
  
    showLoadingBox() {
      let popup = this.popupManager.open('', '', {
        width: '250px',
        injectComponent: LoadingComponent,
        showTitle: false,
        showMessage: false,
        showCloseBtn: false,
        closeOnOverlay: false,
        animate: 'slide-from-bottom',
        actionButtons: [],
        callback: (result: any) => {
          if (result) {
            this.deleteSuccessBox();
          }
        }
      });
  
      // some async call & then close
      setTimeout(() => {
        popup.close(true);
      }, 2000);
    }
  
    deleteSuccessBox() {
      this.popupManager.open('Success', 'Record has been deleted successfully !',         {
        width: '300px',
        animate: 'slide-from-top',
        position: 'top',
        actionButtons: [
          {
            text: 'Done',
            buttonClasses: 'btn-ok'
          }
        ]
      });
    }
    
}

