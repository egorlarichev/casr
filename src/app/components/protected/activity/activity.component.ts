import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, Input, SimpleChange, SimpleChanges } from '@angular/core';
import { Router, ActivatedRoute, Params, Data } from '@angular/router';

import { ActivityService } from '../../../services/activity.service';
import { TranslateService } from '../../../services/translate.service';
import { Activity } from '../../../models/activity'
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import { merge } from "rxjs";
import { tap, delay } from "rxjs/operators";
import { ClearFilterSubject } from '../../../services/subjects.service';
import { BreadcrumbService } from '../../../services/breadcrumb.service'


@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit, AfterViewInit {
  language = this.translateService.getLanguage();

  activityService: ActivityService
  activityList: Activity[];
  dataSource = new MatTableDataSource();
  filterObject: any;
  pageHeader: string = this.route.snapshot.data['pageHeader']
  typeId: number = this.route.snapshot.data['typeId']
  clearFilterSubject: ClearFilterSubject

  IsUrgent: string = "Is Urgent";
  Name: string = "Name";
  FormType: string = "Form Type";
  RequestNumber: string = "Request Number";
  StartDate: string = "Start Date";
  ToBeDoneBy: string = "To Be Done By";
  ConfirmedDate: string = "Confirmed Date";
  ApplicantDate: string = "Applicant Date";
  Entity: string = "Entity";
  Page: string = "";
  breadcrumb$: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ["IsUrgent", "Name", "FormType", "RequestNumber", "StartDate", "ToBeDoneBy", "ConfirmedDate", "Applicant", "Entity"];

  constructor(activityService: ActivityService,
    private route: ActivatedRoute,
    private router: Router,
    clearFilterSubject: ClearFilterSubject,
    private _breadcrumbService: BreadcrumbService,
    private translateService: TranslateService) {

    this.activityService = activityService;
    this.clearFilterSubject = clearFilterSubject;

  }

  ngOnInit() {
    this.filterObject = {
      typeId: this.typeId,
      CurrentPage: 1,
      PageSize: 3,
      Items: 0,
      SortColumn: "",
      SortDirection: "",
      Filter: {},
    }

    this.breadcrumb$ = [{
      label: this.pageHeader,
      url: this.router.url,
    },]

    // this._breadcrumbService.onBreadcrumbChanged.next(breadcrumbData)


    this.activityService.getActivity().subscribe(
      data => {
        this.dataSource.data = data
      }
    );

    this.translateService.onUseLanguage.subscribe(
      language => this.onUsedLanguage(language)
    )
    this.onUsedLanguage(this.language);
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => {
          this.filterObject.CurrentPage = this.paginator.pageIndex;
          this.filterObject.PageSize = this.paginator.pageSize;
          this.filterObject.Items = this.paginator._length;
          this.filterObject.SortColumn = this.sort.active;
          this.filterObject.SortDirection = this.sort.direction;
        }
        )
      )
      .subscribe();

    this.sort.sortChange.subscribe(() => {
      console.log(this.paginator)
      this.paginator.pageIndex = 0
    });

    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  onRowClicked(row) {
    console.log(this.router)
    this.router.navigate([this.router.url + '/details/' + row.Id]);
  }

  onExportToExelClick() {
    alert('You want to export ' + this.pageHeader+'?')
  }


  onApplyFilter(data) {
    this.filterObject.Filter[data.fieldName] = data.filterData
  }

  onClearFilter(data) {
    delete this.filterObject.Filter[data];
  }

  clearAllFilters() {
    var object = {}
    this.clearFilterSubject.clearFilter(object);
    this.filterObject.Filter = {};
  }

  private onUsedLanguage(language: any) {
    this.IsUrgent = language["IsUrgent"];
    this.Name = language["Name"];
    this.FormType = language["FormType"];
    this.RequestNumber = language["RequestNumber"];
    this.StartDate = language["StartDate"];
    this.ToBeDoneBy = language["ToBeDoneBy"];
    this.ConfirmedDate = language["ConfirmedDate"];
    this.ApplicantDate = language["ApplicantDate"];
    this.Entity = language["Entity"];
    this.ConfirmedDate = language["ConfirmedDate"];
    this.ApplicantDate = language["ApplicantDate"];
    this.Page = language[this.pageHeader.replace(/\s/g, "")];
  }
}
