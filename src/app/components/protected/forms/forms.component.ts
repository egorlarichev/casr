import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params, Data } from '@angular/router';
import { FormsService } from '../../../services/forms.service';
import { Form } from '../../../models/form'
import { TranslateService } from '../../../services/translate.service';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {
  language = this.translateService.getLanguage();
  formsService: FormsService;
  formList: Form[];

  criterias = [{
    Name: "formName",
    PrintableName: "Form Name"
  },
  {
    Name: "subFormName",
    PrintableName: "Subform Name"
  },
  {
    Name: "forsmName",
    PrintableName: "Formasasda Name"
  },
  {
    Name: "subForadasdmName9",
    PrintableName: "Subform 123"
  }]

  searchFilter = {};

  ChooseCriterias: string = "Choose criterias";
  TypeSomething: string = "Type something...";
  AvailiableForms: string = "Availiable Forms";
  Submit: string = "Submit";
  Search: string = "Search";

  constructor(formsService: FormsService,
    private translateService: TranslateService) { 
    this.formsService = formsService;
    }

   
  ngOnInit() {
    this.translateService.onUseLanguage.subscribe(
      language => this.onUsedLanguage(language)
    )
    this.onUsedLanguage(this.language);
  }

  getForms() {
    console.log(this.searchFilter)

    this.formsService.getForms().subscribe(
      data => {
        this.formList = data
        console.log(data);
      }
    )
  }
  
  private onUsedLanguage(language: any){
      this.ChooseCriterias =  language["ChooseCriterias"];
      this.TypeSomething = language["TypeSomething"];
      this.AvailiableForms = language["AvailiableForms"];
      this.Submit = language["Submit"];
      this.Search = language["Search"];
    }
} 

