import { Component, OnInit,Input } from '@angular/core';
import { Router, ActivatedRoute, Params, Data } from '@angular/router';
import { ActivityService } from '../../../services/activity.service';
import { Activity } from '../../../models/activity'
import { TranslateService } from '../../../services/translate.service';

@Component({
  selector: 'app-activity-detail',
  templateUrl: './activity-detail.component.html',
  styleUrls: ['./activity-detail.component.css']
})
export class ActivityDetailComponent implements OnInit {
  language = this.translateService.getLanguage();

  activityService: ActivityService;
  activityId: string = this.route.snapshot.params['id'];
  activityDetail: Activity; 
  pageHeader: string = this.route.snapshot.data['pageHeader']
  typeId: number = this.route.snapshot.data['typeId']
  breadcrumb: string = this.route.snapshot.data['breadcrumb']

  breadcrumb$: any;

  IsUrgent: string = "Is Urgent";
  Name: string = "Name";
  FormType: string = "Form Type";
  RequestNumber: string = "Request Number";
  StartDate: string = "Start Date";
  ToBeDoneBy: string = "To Be Done By";
  ConfirmedDate: string = "Confirmed Date";
  ApplicantDate: string = "Applicant Date";
  Entity: string = "Entity";
  Page:string = "";

  constructor(activityService: ActivityService,
    private route: ActivatedRoute,
    private router: Router,
    private translateService: TranslateService) {
    this.activityService = activityService;
  }

  ngOnInit() {
    this.activityService.getActivityDetail(this.activityId).subscribe(
      data => {
        this.activityDetail = data
        console.log(data);
      }
    )
    // this.onUsedLanguage(this.translateService.data);
    this.translateService.onUseLanguage.subscribe(
      language => this.onUsedLanguage(language)
    )
    this.onUsedLanguage(this.language);

    var res = this.router.url.split("/");
    var string = res[0]+'/'+res[1]+'/'+res[2]

 console.log(res,this.router.url, string )

    this.breadcrumb$ = [{
      label: this.pageHeader,
      url: string
    },
    {
      label: this.breadcrumb,
      url: this.router.url,
      params: this.activityId
    },]

  }
  
  private onUsedLanguage(language: any){
    this.IsUrgent =  language["IsUrgent"];
    this.Name = language["Name"];
    this.FormType = language["FormType"];
    this.RequestNumber = language["RequestNumber"];
    this.StartDate = language["StartDate"];
    this.ToBeDoneBy  = language["ToBeDoneBy"];
    this.ConfirmedDate  = language["ConfirmedDate"];
    this.ApplicantDate  = language["ApplicantDate"];
    this.Entity  = language["Entity"];
    this.ConfirmedDate = language["ConfirmedDate"];
    this.ApplicantDate = language["ApplicantDate"];
    this.Entity = language["Entity"];
    this.Page = language[this.pageHeader.replace(/\s/g, "")];
  }
}
