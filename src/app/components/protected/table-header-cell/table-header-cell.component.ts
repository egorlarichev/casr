import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ClearFilterSubject } from '../../../services/subjects.service';
import { TranslateService } from '../../../services/translate.service';

@Component({
  selector: 'app-table-header-cell',
  templateUrl: './table-header-cell.component.html',
  styleUrls: ['./table-header-cell.component.css']
})
export class TableHeaderCellComponent implements OnInit {
  language = this.translateService.getLanguage();
  @Input() cellName;
  @Input() fieldName;
  @Output() emitApplyFilter: EventEmitter<any> = new EventEmitter();
  @Output() emitClearFilter: EventEmitter<any> = new EventEmitter();

  containsData: boolean = false;
  notContainsData: boolean = false;
  filter: boolean = false;
  group: boolean = false;

  searchString: string = "";
  isMenuOpen: boolean = false;
  HaveFilter: boolean = false;
  clearFilterSubject: ClearFilterSubject

  Group: string = "Group";
  ContainsData: string = "Contains Data";
  DoesntContainsData: string = "Doesn`t Contains Data";
  Filter: string = "Filter";
  Clear: string = "Clear";
  Apply: string = "Apply";
  ContainsTheFollowingString: string = "Contains The Following String";

  constructor(
    clearFilterSubject: ClearFilterSubject,
    private translateService: TranslateService
  ) {
    this.clearFilterSubject = clearFilterSubject
  }

  ngOnInit() {

    this.clearFilterSubject.detectFilterClear().subscribe(data => {
      console.log('clearFilter')
      // this.getNews()
      this.onClear();
    })

    this.translateService.onUseLanguage.subscribe(
      language => this.onUsedLanguage(language)
    )
    this.onUsedLanguage(this.language);
  }


  onChangeContainsData() {
    if (this.containsData) {
      this.notContainsData = false;
      this.filter = false;
      this.group = false;
    }
  }

  onChangeNotContainsData() {
    if (this.notContainsData) {
      this.containsData = false;
      this.filter = false;
      this.group = false;
    }
  }

  onFilterChange() {
    if (this.filter) {
      this.containsData = false;
      this.notContainsData = false;
      this.group = false;
    }
  }

  onGroupChange() {
    if (this.group) {
      this.containsData = false;
      this.notContainsData = false;
      this.filter = false;
    }
  }

  onClear() {
    var _this = this;

    this.HaveFilter = false
    this.containsData = false;
    this.notContainsData = false;
    this.filter = false;
    this.group = false;
    this.searchString = "";

    _this.emitClearFilter.emit(this.fieldName);
  }

  onApplyFilter() {
    var _this = this;
    this.HaveFilter = true

    let FilterObject = {
      fieldName: this.fieldName,
      filterData: {
        containsData: this.containsData,
        notContainsData: this.notContainsData,
        filter: this.filter,
        group: this.group,
        searchString: this.searchString,
      }
    }

    _this.emitApplyFilter.emit(FilterObject);

  }

  onMenuChange() {
    this.isMenuOpen = !this.isMenuOpen
    console.log(this.isMenuOpen);

  }

  onUsedLanguage(language: any){
    this.Group =  language["Group"];
    this.ContainsData = language["ContainsData"];
    this.DoesntContainsData = language["DoesntContainsData"];
    this.Filter = language["Filter"];
    this.Clear  = language["Clear"];
    this.Apply  = language["Apply"];
    this.ContainsTheFollowingString = language["ContainsTheFollowingString"];
  }
}
