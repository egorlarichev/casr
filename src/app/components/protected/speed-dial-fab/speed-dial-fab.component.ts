import { Component, OnInit } from '@angular/core';
import { PopupManager } from 'ng6-popup-boxes';


export class LoadingComponent { }

@Component({
  selector: 'app-speed-dial-fab',
  templateUrl: './speed-dial-fab.component.html',
  styleUrls: ['./speed-dial-fab.component.css']
})
export class SpeedDialFabComponent {
  

  fabButtons = [
    {
      icon: 'timeline'
    },
    {
      icon: 'view_headline'
    },
    {
      icon: 'room'
    },
    {
      icon: 'lightbulb_outline'
    },
    {
      icon: 'lock'
    }
  ];
  buttons = [];
  fabTogglerState = 'inactive';

  constructor(public popupManager: PopupManager) { }

  showItems() {
    this.fabTogglerState = 'active';
    this.buttons = this.fabButtons;
  }

  hideItems() {
    this.fabTogglerState = 'inactive';
    this.buttons = [];
  }

  onToggleFab() {
    this.buttons.length ? this.hideItems() : this.showItems();
  }
 
 
 
    showAlertBox() {
      this.popupManager.open(
        'Dynamic help Title',
           'Coming Soon',
        {
          width: '600px',
          popupClass: 'my-popup-box',
          animate: 'slide-from-top',
          showOverlay: true,
          position: 'top',
          callback: (result: any) => {
            if (result) {
              // this.customDialog('You clicked Ok');
            } else {
              // this.customDialog('You clicked Cancel');
            }
          }
        }
      );
    }
  
    customDialog(message: any) {
      this.popupManager.open('Custom Dialog', message, {
        width: '300px',
        position: 'bottom',
        animate: 'scale',
        actionButtons: [
          {
            text: 'Done',
            buttonClasses: 'btn-ok'
          }
        ]
      });
    }
  
    showConfirmBox() {
      this.popupManager.open('Delete Confirmation', 'Do you really want to this item?', {
        width: '300px',
        closeOnOverlay: false,
        animate: 'scale',
        actionButtons: [
          {
            text: 'Yes',
            buttonClasses: 'btn-ok',
            onAction: () => {
              return true;
            }
          },
          {
            text: 'No',
            buttonClasses: 'btn-cancel',
            onAction: () => {
              return false;
            }
          }
        ],
        callback: (result: any) => {
          if (result) {
            this.showLoadingBox();
          }
        }
      });
    }
  
    showLoadingBox() {
      let popup = this.popupManager.open('', '', {
        width: '250px',
        injectComponent: LoadingComponent,
        showTitle: false,
        showMessage: false,
        showCloseBtn: false,
        closeOnOverlay: false,
        animate: 'slide-from-bottom',
        actionButtons: [],
        callback: (result: any) => {
          if (result) {
            this.deleteSuccessBox();
          }
        }
      });
  
      // some async call & then close
      setTimeout(() => {
        popup.close(true);
      }, 2000);
    }
  
    deleteSuccessBox() {
      this.popupManager.open('Success', 'Record has been deleted successfully !',         {
        width: '300px',
        animate: 'slide-from-top',
        position: 'top',
        actionButtons: [
          {
            text: 'Done',
            buttonClasses: 'btn-ok'
          }
        ]
      });
    }

} 

