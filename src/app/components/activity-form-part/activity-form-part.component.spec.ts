import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityFormPartComponent } from './activity-form-part.component';

describe('ActivityFormPartComponent', () => {
  let component: ActivityFormPartComponent;
  let fixture: ComponentFixture<ActivityFormPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityFormPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityFormPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
