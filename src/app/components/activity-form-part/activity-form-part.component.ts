import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '../../services/translate.service';

@Component({
  selector: 'app-activity-form-part',
  templateUrl: './activity-form-part.component.html',
  styleUrls: ['./activity-form-part.component.css']
})
export class ActivityFormPartComponent implements OnInit {
  language = this.translateService.getLanguage();
  
  @Input() setActivityDetail;

  RequestNumber: string = "Request Number";
  Name:string = "Name";
  FormType: string = "Form Type";
  ToBeDoneBy: string = "To Be Done By";
  AssignedDate: string = "Assigned Date";
  AssignedTime: string = "Assigned Time";
  CompletedDate: string = "Completed Date";
  CompletedTime: string = "Completed Time";
  Observation: string = "Observation";
  
  constructor(private translateService: TranslateService) { }

  ngOnInit() {
    this.translateService.onUseLanguage.subscribe(
      language => this.onUsedLanguage(language)
    )
    this.onUsedLanguage(this.language);
  }

  private onUsedLanguage(language: any){
    this.RequestNumber =  language["RequestNumber"];
    this.Name = language["Name"];
    this.FormType = language["FormType"];
    this.ToBeDoneBy = language["ToBeDoneBy"];
    this.AssignedDate = language["AssignedDate"];
    this.AssignedTime = language["AssignedTime"];
    this.CompletedDate = language["CompletedDate"];
    this.CompletedTime = language["CompletedTime"];
    this.Observation = language["Observation"];
  }

}
