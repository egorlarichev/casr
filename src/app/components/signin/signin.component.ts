import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { LoaderService } from '../../services/loader.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '../../services/translate.service';

export interface Language{
  value: string;
  valueName: string;
}

@Component({
  selector: 'irms-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})

export class SigninComponent implements OnInit {
  language = this.translateService.getLanguage();
  public loginForm: FormGroup;
  lang: string = "Choose language";

  isLanguageOpen: boolean = false;
  Email: string =  "Email";
  Password: string = "Password";

  constructor(
    private route: ActivatedRoute, 
    private router: Router, 
    private loader: LoaderService, 
    private authService: AuthService,
    private fb: FormBuilder,
    private translateService: TranslateService
  ) {
    this.loginForm = this.fb.group({
      'email': ['', Validators.required],
      'password': ['', Validators.required]
    });
  }

  error: string;

  ngOnInit() {
    this.translateService.onUseLanguage.subscribe(
      language => this.onUsedLanguage(language)
    )
    this.onUsedLanguage(this.language);
  }

  SetLanguage(language: string){
    this.translateService.use(language);
    this.lang = language;
  }

  Login() {
    this.loader.start();

    const credentials = this.loginForm.value;

    this.authService.signIn(credentials)
      .then(response => {
        console.log(2)
        this.loader.stop();
        this.router.navigate(['./sec']);
      })
      .catch( error => {
        console.log(1)
        this.loader.stop();
        this.error = error.message;
      });

  }

  openLanguageMenu(){
    this.isLanguageOpen = !this.isLanguageOpen
  }

  private onUsedLanguage(language: any){
    this.Email =  language["Email"];
    this.Password = language["Password"];
  }

}



