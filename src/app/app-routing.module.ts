import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//Components
import { HomeComponent } from './components/home/home.component';
import { MyRequestsComponent } from './components/protected/my-requests/my-requests.component';
import { FormListComponent } from './components/form-list/form-list.component';
import { DemoComponent } from 'casr-forms';
import { SigninComponent } from './components/signin/signin.component';
import { ProtectedComponent } from './components/protected/protected.component';
import { ActivityComponent } from './components/protected/activity/activity.component';
import { ActivityDetailComponent } from './components/protected/activity-detail/activity-detail.component';
import { RequestDetailComponent } from './components/protected/request-detail/request-detail.component';
import { FormsComponent } from './components/protected/forms/forms.component';
import { HelpComponent } from './components/protected/help/help.component';
import { ChangePasComponent } from './components/protected/changePas/changePas.component';


//Guards
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, data: { breadcrumb: 'home' } },
  { path: 'signin', component: SigninComponent, data: { breadcrumb: 'signin' } },
  {
    path: 'sec', component: ProtectedComponent, data: { breadcrumb: 'Dashboard' }, canActivate: [AuthGuard], children: [
      { path: '', redirectTo: '/sec/mywork', pathMatch: 'full' },

      {
        path: 'mywork', component: ActivityComponent, data: { pageHeader: 'My Work', typeId: 1, breadcrumb: 'My Work' }, children: [
        ]
      },

      { path: 'mywork/details/:id', component: ActivityDetailComponent, data: { pageHeader: 'My Work', typeId: 1, breadcrumb: 'Details' } },

      {
        path: 'unassigned', component: ActivityComponent, data: { pageHeader: 'Unassigned', typeId: 2, breadcrumb: 'Unassigned' }, children: [
        ]
      },

      { path: 'unassigned/details/:id', component: ActivityDetailComponent, data: { pageHeader: 'Unassigned', typeId: 2, breadcrumb: 'Details' } },




      {
        path: 'team', component: ActivityComponent, data: { pageHeader: 'Team', typeId: 3, breadcrumb: 'Team' }, children: [
        ]
      },

      { path: 'team/details/:id', component: ActivityDetailComponent, data: { pageHeader: 'Team', typeId: 3, breadcrumb: 'Details' } },

      {
        path: 'completed', component: ActivityComponent, data: { pageHeader: 'Completed', typeId: 4, breadcrumb: 'Completed' }, children: [
        ]
      },

      { path: 'completed/details/:id', component: ActivityDetailComponent, data: { pageHeader: 'Completed', typeId: 4, breadcrumb: 'Details' } },


      {
        path: 'open', component: ActivityComponent, data: { pageHeader: 'Open', typeId: 5, breadcrumb: 'Open' }, children: [
        ]
      },

      { path: 'open/details/:id', component: ActivityDetailComponent, data: { pageHeader: 'Open  ', typeId: 4, breadcrumb: 'Details' } },


      {
        path: 'requests', component: MyRequestsComponent, data: { pageHeader: 'Requests', breadcrumb: 'Requests' }, children: [
        ]
      },

      { path: 'requests/details/:id', component: RequestDetailComponent, data: { pageHeader: 'Requests', typeId: 4, breadcrumb: 'Details' } },



      { path: 'help', component: HelpComponent, data: { pageHeader: 'Help', typeId: 6, breadcrumb: 'help' } },
      { path: 'changePas', component: ChangePasComponent, data: { pageHeader: 'Change password', typeId: 7, breadcrumb: 'changePass' } },

      { path: 'forms', component: FormsComponent, data: { breadcrumb: 'forms' } },


      // { path: 'forms', component: FormListComponent },
      { path: 'forms/demo', component: DemoComponent },
      // { path: '**', redirectTo: '/sec/mywork' },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
