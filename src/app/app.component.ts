import { Component } from '@angular/core';
import { TranslateService } from './services/translate.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //title = 'casr-portal';

  constructor(private translate: TranslateService, private titleService: Title){
    
    console.log(translate.data);
    this.titleService.setTitle(translate.data['TITLE']);
  }
}
