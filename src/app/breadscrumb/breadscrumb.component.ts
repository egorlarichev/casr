import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { BreadCrumb } from './breadscrumb';
import { filter } from 'rxjs/operators';
import {BreadcrumbService} from '../services/breadcrumb.service'
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadscrumb.component.html',
  styleUrls: ['./breadscrumb.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BreadscrumbComponent implements OnInit {
  breadcrumb$: Observable<Array<any>>;


 
    // .map(event => this.buildBreadCrumb(this.activatedRoute.root));
  // Build your breadcrumb starting with the root route of your current activated route
  constructor(private activatedRoute: ActivatedRoute,
    private _breadcrumbService: BreadcrumbService,
    private router: Router) {
  }

  ngOnInit() {
   this._breadcrumbService.onBreadcrumbChanged.subscribe(
    //  data => {
    //    this.breadcrumb$ = [];
    //    this.breadcrumb$.push(data)
    //    this.breadcrumb$.slice();
    //    console.log(this.breadcrumb$)
    //  }
   )
  }

  // this.breadcrumb = this.buildBreadCrumb()
//   buildBreadCrumb(route: ActivatedRoute, url: string = '',
//     breadcrumbs: Array<BreadCrumb> = []): Array<BreadCrumb> {
//     // If no routeConfig is avalailable we are on the root path
//     console.log(route)
//     const label = route.routeConfig ? route.routeConfig.data['breadcrumb'] : 'Home';
//     const path = route.routeConfig ? route.routeConfig.path : '';
//     const params  = route.params['value'].id ? route.params['value'].id : '';
//     // In the routeConfig the complete path is not available,
//     // so we rebuild it each time
//     const nextUrl = `${url}${path}/`;
//     const breadcrumb = {
//       label: label,
//       url: nextUrl,
//       params: params
//     };
//     const newBreadcrumbs = [...breadcrumbs, breadcrumb];
//     if (route.firstChild) {
//       // If we are not on our current path yet,
//       // there will be more children to look after, to build our breadcumb
//       return this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);
//     }
//     return newBreadcrumbs;
//   }
// }

 // this.breadcrumb$ = this.buildBreadCrumb(this.activatedRoute.root)

    // this.router.events.pipe(
    //   filter((event:Event) => event instanceof NavigationEnd)
    // ).subscribe(x => {
    //   this.breadcrumb$ = this.buildBreadCrumb(this.activatedRoute.root)
    //   console.log("bread", this.breadcrumb$)
    // }
      
    //   )
}