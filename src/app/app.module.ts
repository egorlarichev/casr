//Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatModule } from './shared/matmodule/matmodule.module';
import { FormsModule  as AngularForms} from '@angular/forms' ;
import { HttpModule } from '@angular/http';
import {  ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import {BreadcrumbsModule} from 'ng6-breadcrumbs';
import { PopupBoxesModule } from 'ng6-popup-boxes';
import { MatButtonModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';

  //Custom
import { FormsModule } from 'casr-forms';
//

//System
import { AppRoutingModule } from './app-routing.module';

//Guards
import { AuthGuard } from './guards/auth.guard';

//Components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SigninComponent } from './components/signin/signin.component';
import { ProtectedComponent } from './components/protected/protected.component';
import { LoaderComponent } from './components/loader/loader.component';
import { ActivityComponent } from './components/protected/activity/activity.component';
import { TableHeaderCellComponent } from './components/protected/table-header-cell/table-header-cell.component';
import { ActivityDetailComponent } from './components/protected/activity-detail/activity-detail.component';
import { ActivityFormPartComponent } from './components/activity-form-part/activity-form-part.component';
import { RequestDetailComponent } from './components/protected/request-detail/request-detail.component';
import { FormsComponent } from './components/protected/forms/forms.component';

import { TableHeaderDateCellComponent } from './components/protected/table-header-date-cell/table-header-date-cell.component';
import { HelpComponent } from './components/protected/help/help.component';
import { ChangePasComponent } from './components/protected/changePas/changePas.component';



//
import { MyRequestsComponent } from './components/protected/my-requests/my-requests.component';
import { FormListComponent } from './components/form-list/form-list.component';
import { FormContainerComponent } from './components/form-container/form-container.component';

//

//Services
import { AuthService } from './services/auth.service';
import { LoaderService } from './services/loader.service';
import { ProfileService } from './services/profile.service';
import { RequestService } from './services/request.service';
import { ActivityService } from './services/activity.service';
import { FormsService } from './services/forms.service';
import { TranslateService } from './services/translate.service';
import { BreadcrumbService } from './services/breadcrumb.service';

//Subjects
import { ClearFilterSubject } from './services/subjects.service';


//DB
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { Router } from '@angular/router';
import { SpeedDialFabComponent } from './components/protected/speed-dial-fab/speed-dial-fab.component';
import { TranslatePipe } from './translate.pipe';
import { BreadscrumbComponent } from './breadscrumb/breadscrumb.component';

export function setupTranslateFactory(service: TranslateService):
Function{return() => service.use('la');}



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SigninComponent,
    ProtectedComponent,
    LoaderComponent, //loading spinner
    MyRequestsComponent,
    FormListComponent,
    FormContainerComponent,
    ActivityComponent,
    TableHeaderCellComponent,
    ActivityDetailComponent,
    ActivityFormPartComponent,
    RequestDetailComponent,
    FormsComponent,
    TableHeaderDateCellComponent,
    HelpComponent,
    ChangePasComponent,
    SpeedDialFabComponent,
  
    TranslatePipe,
    TableHeaderDateCellComponent,
    BreadscrumbComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    AngularForms,
    AppRoutingModule,
    FormsModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule, 
    AngularFireStorageModule,
    BreadcrumbsModule,
    NgSelectModule,
    PopupBoxesModule.forRoot(),
    MatButtonModule,
    HttpClientModule,
    NgSelectModule,
  ],
  providers: [
    AuthGuard,
    RequestService,
    ProfileService,
    LoaderService,
    AuthService,
    ActivityService,
    FormsService,
    BreadcrumbService,
    ClearFilterSubject,

    TranslateService,
    {
      provide: APP_INITIALIZER,
      useFactory: setupTranslateFactory,
      deps: [TranslateService],
      multi: true
    },
    ClearFilterSubject
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
