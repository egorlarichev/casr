pipeline {
    agent {
        label 'java8'
    }
    stages {
        stage('Copy and Install casr-forms artifact') {
            steps {
                copyArtifacts(projectName: '../casr-forms/' + env.BRANCH_NAME, filter: '*.tgz')
                sh "npm install *.tgz"
            }
        }
        stage('Fetch dependencies') {
            steps {
                sh 'npm install'
            }
        }
        stage('Lint') {
            steps {
                sh 'npm run lint'
            }
        }
        stage('Test') {
            steps {
                sh 'npm run test:ci'
                junit 'reports/**/*.xml'
            }
        }
        stage('Build') {
            steps {
                sh 'npm run build -- --prod --progress=false --base-href=/casr-api/casr-portal/'
                sh 'tar -cvzf casr-portal.dist.tar.gz --strip-components=1 dist'
                archiveArtifacts artifacts: 'casr-portal.dist.tar.gz', fingerprint: true
            }
        }
        stage('Trigger casr-api build') {
            steps {
                script {
                    try {
                        build(job: 'casr-api/' + env.BRANCH_NAME, wait: false)
                    } catch (err) {
                        echo "casr-api/" + env.BRANCH_NAME + " not found"
                    }
                }
            }
        }
    }
    options {
        buildDiscarder(logRotator(numToKeepStr: '5'))
        timeout(time: 10, unit: 'MINUTES')
    }
}
